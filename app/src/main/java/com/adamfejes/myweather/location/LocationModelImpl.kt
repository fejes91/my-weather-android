package com.adamfejes.myweather.location

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.content.ContextCompat

import android.content.Context.LOCATION_SERVICE


/**
 * Implementation of [LocationModel]
 * Created by Adam Fejes on 2018. 03. 26..
 */

class LocationModelImpl(private val activity: Activity) : LocationModel {
    private val locationListener: LocationListener
    private val locationManager: LocationManager = activity.getSystemService(LOCATION_SERVICE) as LocationManager
    override val locationLiveData: MutableLiveData<Location> = MutableLiveData()
    private val inprogressLiveData: MutableLiveData<Boolean> = MutableLiveData()

    override//TODO check if location is enabled on device
    val isLocationAvailable: Boolean
        get() = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED

    override val inProgressLiveData: LiveData<Boolean>
        get() = inprogressLiveData

    init {
        locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                locationLiveData.value = location
                inprogressLiveData.value = false
            }

            override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {
                //TODO
            }

            override fun onProviderEnabled(s: String) {
                //TODO
            }

            override fun onProviderDisabled(s: String) {
                //TODO
            }
        }
    }//TODO access coarse location too

    override fun requestLocation() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 1000f, locationListener) //TODO check params //TODO no magic numbers
            inprogressLiveData.value = true
            //TODO timeout for location
        }
    }
}
