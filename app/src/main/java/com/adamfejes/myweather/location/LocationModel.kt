package com.adamfejes.myweather.location

import android.arch.lifecycle.LiveData
import android.location.Location

/**
 * Model class for location services
 * Created by Adam Fejes on 2018. 03. 26..
 */

interface LocationModel {
    val isLocationAvailable: Boolean
    val locationLiveData: LiveData<Location>
    val inProgressLiveData: LiveData<Boolean>
    fun requestLocation()
}
