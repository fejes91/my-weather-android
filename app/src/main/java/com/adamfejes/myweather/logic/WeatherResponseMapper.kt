package com.adamfejes.myweather.logic

import android.content.res.Resources

import com.adamfejes.myweather.R
import com.adamfejes.myweather.db.WeatherEntity
import com.adamfejes.myweather.network.Response

import java.util.Date

/**
 * Mapper class for [Response] to [WeatherEntity] mappings
 * Created by Adam_Fejes on 2018-03-27.
 */

class WeatherResponseMapper(private val resources: Resources) {

    fun mapWeatherEntity(response: Response): WeatherEntity {
        val entity = WeatherEntity()
        if (response.weather?.isNotEmpty() == true) {
            entity.condition = response.weather!![0].description
        }
        entity.temperature = getTemperatureInCelsius(response)
        entity.windSpeed = response.wind?.speed
        entity.windDirection = getWindDirection(response)
        entity.updatedAt = Date()
        entity.weatherIconUrl = getAbsoluteIconUrl(response)
        return entity
    }

    private fun getWindDirection(response: Response): String? {
        var degrees: Double? = response.wind?.deg?.rem(360)
        if(degrees == null) {
            return null
        }

        if (degrees < 0) {
            degrees = 360 + degrees!!
        }

        return if (degrees < 22.5) {
            "N"
        } else if (degrees < 67.5) {
            "NE"
        } else if (degrees < 112.5) {
            "E"
        } else if (degrees < 157.5) {
            "SE"
        } else if (degrees < 202.5) {
            "S"
        } else if (degrees < 247.5) {
            "SW"
        } else if (degrees < 292.5) {
            "W"
        } else if (degrees < 337.5) {
            "NW"
        } else {
            "N"
        }
    }

    private fun getAbsoluteIconUrl(response: Response): String? {
        return if (response.weather?.isNotEmpty() == true) {
            resources.getString(R.string.icon_url_pattern, response.weather!![0].icon)
        } else null

    }

    private fun getTemperatureInCelsius(response: Response): Double? {
        return response.main?.temp?.minus(273.15) ?: null
    }
}
