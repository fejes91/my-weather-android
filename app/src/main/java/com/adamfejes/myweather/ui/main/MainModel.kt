package com.adamfejes.myweather.ui.main

import android.arch.lifecycle.LiveData
import android.location.Location

import com.adamfejes.myweather.db.WeatherEntity

/**
 * Model class for handling data fetching
 * Created by Adam Fejes on 2018. 03. 26..
 */

interface MainModel {
    val weather: LiveData<WeatherEntity>
    val isLoading: LiveData<Boolean>
    var currentLocation: String?
    fun refreshData(location: Location)
    fun refreshDataBeforeLocation()
    fun errorMessage(): LiveData<String>
}
