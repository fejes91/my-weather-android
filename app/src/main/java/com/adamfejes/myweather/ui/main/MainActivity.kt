package com.adamfejes.myweather.ui.main

import android.Manifest
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat

import com.adamfejes.myweather.R
import com.adamfejes.myweather.databinding.ActivityMainBinding

import javax.inject.Inject

import dagger.Lazy
import dagger.android.support.DaggerAppCompatActivity

/**
 * Activity for main screen
 */
class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var mainViewModel: Lazy<MainViewModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityMainBinding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        activityMainBinding.viewModel = mainViewModel.get()
        setSupportActionBar(activityMainBinding.toolbar)
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE)
        }
    }

    companion object {
        val REQUEST_CODE = 1
    }

    //TODO handle request denial
}
