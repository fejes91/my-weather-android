package com.adamfejes.myweather.ui.main

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.databinding.BaseObservable
import android.graphics.Color
import android.location.Location
import android.support.annotation.ColorRes
import android.view.View

import com.adamfejes.Utils
import com.adamfejes.myweather.BR
import com.adamfejes.myweather.R
import com.adamfejes.myweather.db.WeatherEntity
import com.adamfejes.myweather.location.LocationModel

import java.text.DateFormat

/**
 * Implements [MainViewModel]
 * Created by Adam Fejes on 2018. 03. 25..
 */

class MainViewModelImpl(lifecycleOwner: LifecycleOwner, private val utils: Utils, private val mainModel: MainModel, private val locationModel: LocationModel) : BaseObservable(), MainViewModel {
    private val DOUBLE_FORMAT = "%.1f"
    private val dateFormat: DateFormat = DateFormat.getDateTimeInstance()
    private var weatherEntity: WeatherEntity? = null
    private var isLoading: Boolean? = false
    private var errorMessage: String? = null
    private var locationRequestInProgress: Boolean? = null

    override val location: String
        get() = mainModel.currentLocation ?: "unknown"

    override val condition: String
        get() = weatherEntity?.condition ?: "-"

    override val temperature: String
        get() = if(weatherEntity?.temperature != null) {
            String.format(DOUBLE_FORMAT, weatherEntity?.temperature)
        } else "-"


    override val windSpeed: String
        get() = if (weatherEntity?.windSpeed != null) {
            String.format(DOUBLE_FORMAT, weatherEntity!!.windSpeed)
        } else "-"

    override val windDirection: String
        get() = weatherEntity?.windDirection ?: "-"

    override val updated: String
        get() = if (weatherEntity?.updatedAt != null) {
            dateFormat.format(weatherEntity!!.updatedAt)
        } else "-"

    override val progressBarVisibility: Int
        get() = if (isLoading == true) View.VISIBLE else View.GONE

    override val dateVisibilty: Int
        get() = if (isLoading == true) View.GONE else View.VISIBLE

    override val dateColor: Int
        get() = if (errorMessage?.isNotEmpty() == true) Color.RED else Color.BLACK


    override val iconUrl: String?
        get() = weatherEntity?.weatherIconUrl

    override val satIconColor: Int
        @ColorRes
        get() = when {
            locationRequestInProgress == null -> R.color.gray
            locationRequestInProgress!! -> R.color.yellow
            else -> R.color.green
        }

    init {

        this.mainModel.weather.observe(lifecycleOwner, Observer<WeatherEntity> { weatherEntity -> this.handleWeatherEntity(weatherEntity) })
        this.mainModel.isLoading.observe(lifecycleOwner, Observer<Boolean> { isLoading -> this.handleLoadingState(isLoading) })
        this.mainModel.errorMessage().observe(lifecycleOwner, Observer<String> { errorMessage -> this.handleErrorState(errorMessage) })
        this.locationModel.locationLiveData.observe(lifecycleOwner, Observer<Location> { locationModel -> this.handleLocation(locationModel) })
        this.locationModel.inProgressLiveData.observe(lifecycleOwner, Observer<Boolean> { isLocationRequestInProgress -> this.handleLocationRequestStatus(isLocationRequestInProgress) })

        requestLocation()
    }

    private fun handleWeatherEntity(weatherEntity: WeatherEntity?) {
        if (weatherEntity == null) {
            handleErrorState("Unexpected error")
        }
        this.weatherEntity = weatherEntity
        notifyPropertyChanged(BR.condition)
        notifyPropertyChanged(BR.temperature)
        notifyPropertyChanged(BR.windDirection)
        notifyPropertyChanged(BR.windSpeed)
        notifyPropertyChanged(BR.iconUrl)
        notifyPropertyChanged(BR.updated)
        notifyPropertyChanged(BR.location)
    }

    private fun handleLoadingState(isLoading: Boolean?) {
        this.isLoading = isLoading
        notifyPropertyChanged(BR.dateVisibilty)
        notifyPropertyChanged(BR.progressBarVisibility)
    }

    private fun handleErrorState(errorMessage: String?) {
        this.errorMessage = errorMessage
        notifyPropertyChanged(BR.dateColor)
    }

    private fun handleLocationRequestStatus(isInProgress: Boolean?) {
        this.locationRequestInProgress = isInProgress
        notifyPropertyChanged(BR.satIconColor)
        notifyPropertyChanged(BR.location)
    }

    private fun handleLocation(location: Location?) {
        if (location != null) {
            mainModel.refreshData(location)
        }
    }

    private fun requestLocation() {
        //TODO display if location request is in progress
        if (locationModel.isLocationAvailable) {
            locationModel.requestLocation()
            isLoading = true
            notifyPropertyChanged(BR.dateVisibilty)
            notifyPropertyChanged(BR.progressBarVisibility)
        }

        notifyPropertyChanged(BR.satIconColor)
    }


    override fun fabClicked(v: View) {
        mainModel.refreshDataBeforeLocation()
        requestLocation()
    }
}
