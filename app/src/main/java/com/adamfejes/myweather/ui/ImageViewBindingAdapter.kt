package com.adamfejes.myweather.ui

import android.databinding.BindingAdapter
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created by Adam Fejes on 2018. 10. 23..
 */
@BindingAdapter("loadUrl")
fun setImageUrl(imageView: ImageView, url: String?) {
    Picasso.with(imageView.context).load(url).into(imageView)
}

@BindingAdapter("ind:tint")
fun setTint(imageView: ImageView, @ColorRes colorRes: Int) {
    imageView.setColorFilter(ContextCompat.getColor(imageView.context, colorRes))
}