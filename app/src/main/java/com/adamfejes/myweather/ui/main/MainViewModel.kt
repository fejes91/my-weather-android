package com.adamfejes.myweather.ui.main

import android.databinding.Bindable
import android.databinding.Observable
import android.view.View

/**
 * Provides [android.databinding.Bindable] resources for the main screen
 * Created by Adam Fejes on 2018. 03. 25..
 */

interface MainViewModel : Observable {
    @get:Bindable
    val location: String
    @get:Bindable
    val condition: String
    @get:Bindable
    val temperature: String
    @get:Bindable
    val windSpeed: String
    @get:Bindable
    val windDirection: String
    @get:Bindable
    val updated: String
    @get:Bindable
    val progressBarVisibility: Int
    @get:Bindable
    val dateVisibilty: Int
    @get:Bindable
    val dateColor: Int
    @get:Bindable
    val iconUrl: String?
    @get:Bindable
    val satIconColor: Int

    fun fabClicked(v: View)
}
