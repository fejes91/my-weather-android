package com.adamfejes.myweather.ui.main

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Location
import android.os.Handler
import android.os.Looper
import android.widget.Toast

import com.adamfejes.myweather.db.WeatherDao
import com.adamfejes.myweather.db.WeatherEntity
import com.adamfejes.myweather.logic.WeatherResponseMapper
import com.adamfejes.myweather.network.ApiManager
import com.adamfejes.myweather.network.Response

import java.util.Date

/**
 * Implementation for [MainModel]
 * Handling network calls and caching
 * Extends AndroidViewModel -> Can be provied by {@Link android.arch.lifecycle.ViewModelProvider}
 * Created by Adam Fejes on 2018. 03. 26..
 */

class MainModelImpl(private val weatherDao: WeatherDao, private val apiManager: ApiManager, private val weatherResponseMapper: WeatherResponseMapper) : ViewModel(), MainModel {
    override val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    private val errorMessage: MutableLiveData<String> = MutableLiveData()
    private var lastKnownLocation: Location? = null
    override var currentLocation: String? = null

    override val weather: LiveData<WeatherEntity>
        get() = weatherDao.getWeather(lastValidDate)


    private val lastValidDate: Date
        get() {
            val twentyFourHoursBefore = Date()
            twentyFourHoursBefore.time = twentyFourHoursBefore.time - 1000 * 60 * 60 * 24
            return twentyFourHoursBefore
        }

    init {
        refreshDataBeforeLocation() //TODO should be called in a lifecycle-like method
    }

    @SuppressLint("CheckResult")
    override fun refreshData(location: Location) {
        lastKnownLocation = location
        currentLocation = location.toString()
        isLoading.value = true
        apiManager.getWeatherByCoords(location.latitude, location.longitude)
                .map {weather -> weatherResponseMapper.mapWeatherEntity(weather) }
                .subscribe({ this.handleWeatherEntity(it) }, { this.handleError(it) })
    }

    @SuppressLint("CheckResult")
    private fun refreshData(city: String) {
        currentLocation = city
        isLoading.value = true
        apiManager.getWeatherByCity(city)
                .map {weather -> weatherResponseMapper.mapWeatherEntity(weather) }
                .subscribe({ this.handleWeatherEntity(it) }, { this.handleError(it) })
    }

    override fun refreshDataBeforeLocation() { //TODO {@link refreshDataBeforeLocation} and {@link refreshData} should be the same
        if (lastKnownLocation != null) {
            refreshData(lastKnownLocation!!)
        } else {
            refreshData("Budapest") //TODO no magic strings
        }
    }

    private fun handleWeatherEntity(weatherEntity: WeatherEntity) {
        weatherDao.storeWeather(weatherEntity)
        errorMessage.postValue(null)
        isLoading.postValue(false)
    }

    private fun handleError(throwable: Throwable) {
        errorMessage.postValue(throwable.message)
        isLoading.postValue(false)
    }

    override fun errorMessage(): LiveData<String> {
        return errorMessage
    }
}
