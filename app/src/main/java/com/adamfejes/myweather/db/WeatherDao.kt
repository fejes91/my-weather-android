package com.adamfejes.myweather.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.TypeConverters

import com.adamfejes.myweather.ui.main.MainViewModelImpl

import java.util.Date

import android.arch.persistence.room.OnConflictStrategy.REPLACE

/** DAO class for weather info. [MainViewModelImpl] observes the LiveData prodived by this DAO.
 * Every change in the table will be emitted to the observers through LiveData, so the DB is the single source of truth in the app.
 * Created by Adam_Fejes on 2018-03-26.
 */

@Dao
@TypeConverters(DateConverter::class)
interface WeatherDao {
    /**
     * Get stored weather info through LiveData if it isn't older than 24 hours
     * @return Observable LiveData. Any changes in this table will be emitted for all the observers.
     */
    @Query("select * from WeatherEntity WHERE updatedAt > :beforeTwentyFourHours")
    fun getWeather(beforeTwentyFourHours: Date): LiveData<WeatherEntity>

    /**
     * Store weather info into database
     * @param weatherEntity weather info to store
     */
    @Insert(onConflict = REPLACE)
    fun storeWeather(weatherEntity: WeatherEntity)
}
