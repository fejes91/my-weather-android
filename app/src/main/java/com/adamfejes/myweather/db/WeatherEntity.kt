package com.adamfejes.myweather.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters

import java.util.Date

/**
 * Entity class for weather info
 * Created by Adam_Fejes on 2018-03-26.
 */

@Entity
class WeatherEntity {
    @PrimaryKey
    var id: Long? = null
    var condition: String? = null
    var temperature: Double? = null
    var windSpeed: Double? = null
    var windDirection: String? = null
    var weatherIconUrl: String? = null
    @TypeConverters(DateConverter::class)
    var updatedAt: Date? = null

    init {
        id = 0L //this should be always the same to store only one record
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val entity = o as WeatherEntity?

        if (id != entity!!.id) return false
        if (condition != entity.condition) return false
        if (temperature != entity.temperature) return false
        if (windSpeed != entity.windSpeed) return false
        if (windDirection != entity.windDirection) return false
        return if (weatherIconUrl != entity.weatherIconUrl) false else updatedAt == entity.updatedAt
    }

    override fun hashCode(): Int {
        var result = id!!.hashCode()
        result = 31 * result + condition!!.hashCode()
        result = 31 * result + temperature!!.hashCode()
        result = 31 * result + windSpeed!!.hashCode()
        result = 31 * result + windDirection!!.hashCode()
        result = 31 * result + weatherIconUrl!!.hashCode()
        result = 31 * result + updatedAt!!.hashCode()
        return result
    }
}
