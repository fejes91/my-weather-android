package com.adamfejes.myweather.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

/**
 * Database singleton
 * Created by Adam_Fejes on 2018-03-26.
 */
@Database(entities = arrayOf(WeatherEntity::class), version = 1)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun weatherDao(): WeatherDao

    companion object {
        private val DB_NAME = "weather_db"
        private var INSTANCE: WeatherDatabase? = null

        fun getDatabase(context: Context): WeatherDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, WeatherDatabase::class.java, DB_NAME).build()
            }
            return INSTANCE!!
        }
    }
}
