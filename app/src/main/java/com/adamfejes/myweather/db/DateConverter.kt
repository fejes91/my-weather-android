package com.adamfejes.myweather.db

import android.arch.persistence.room.TypeConverter

import java.util.Date

/**
 * TypeConverter for [WeatherEntity]
 * Created by Adam Fejes on 2018. 03. 26..
 */

class DateConverter {
    @TypeConverter
    fun toDate(timestamp: Long?): Date? {
        return if (timestamp == null) null else Date(timestamp)
    }

    @TypeConverter
    fun toTimestamp(date: Date?): Long? {
        return date?.time
    }
}
