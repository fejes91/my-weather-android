package com.adamfejes.myweather.di

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders

import com.adamfejes.Utils
import com.adamfejes.myweather.db.WeatherDao
import com.adamfejes.myweather.db.WeatherDatabase
import com.adamfejes.myweather.location.LocationModel
import com.adamfejes.myweather.location.LocationModelImpl
import com.adamfejes.myweather.logic.WeatherResponseMapper
import com.adamfejes.myweather.network.ApiManager
import com.adamfejes.myweather.ui.main.MainActivity
import com.adamfejes.myweather.ui.main.MainModelImpl
import com.adamfejes.myweather.ui.main.MainViewModel
import com.adamfejes.myweather.ui.main.MainViewModelImpl

import dagger.Module
import dagger.Provides

/**
 * Dagger module for main screen
 * Created by Adam Fejes on 2018. 02. 06..
 */

@Module
class MainModule {
    @Provides
    internal fun provideHomePageViewModel(activity: MainActivity,
                                          utils: Utils,
                                          weatherDao: WeatherDao,
                                          apiManager: ApiManager,
                                          locationModel: LocationModel,
                                          weatherResponseMapper: WeatherResponseMapper): MainViewModel {
        return MainViewModelImpl(activity, utils, ViewModelProviders.of(activity, object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return MainModelImpl(weatherDao, apiManager, weatherResponseMapper) as T
            }
        }).get<MainModelImpl>(MainModelImpl::class.java!!), locationModel)
    }

    @Provides
    internal fun provideApiManager(application: Application): ApiManager {
        return ApiManager(application.resources)
    }

    @Provides
    internal fun provideLocationModel(activity: MainActivity): LocationModel {
        return LocationModelImpl(activity)
    }

    @Provides
    internal fun provideRepsonseMapper(application: Application): WeatherResponseMapper {
        return WeatherResponseMapper(application.resources)
    }

    @Provides
    internal fun provideUtils(): Utils {
        return Utils()
    }

    @Provides
    internal fun provideWeatherDao(application: Application): WeatherDao {
        return WeatherDatabase.getDatabase(application).weatherDao()
    }
}
