package com.adamfejes.myweather.di

import android.app.Application
import android.content.Context

import dagger.Binds
import dagger.Module

/**
 * Application level Dagger module
 * Created by Adam Fejes on 2018. 02. 06..
 */

@Module(subcomponents = arrayOf(MainComponent::class))
abstract class AppModule {
    @Binds
    internal abstract fun provideContext(application: Application): Context
}
