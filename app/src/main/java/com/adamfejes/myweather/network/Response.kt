package com.adamfejes.myweather.network

/**
 * Simple entity class for holding network response
 * Created by Adam Fejes on 2018. 03. 26..
 */

class Response {
    var weather: ArrayList<Weather>? = null
    var main: Main? = null
    var wind: Wind? = null

    class Weather {
        var description: String? = null
        var icon: String? = null
    }

    class Main {
        var temp: Double? = null
    }

    class Wind {
        var speed: Double? = null
        var deg: Double? = null
    }
}
