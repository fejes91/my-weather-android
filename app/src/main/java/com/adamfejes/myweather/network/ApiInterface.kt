package com.adamfejes.myweather.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit API interface
 * Created by Adam Fejes on 2018. 03. 26..
 */

interface ApiInterface {
    /**
     * Network call with coordinates as params
     * @param latitude
     * @param longitude
     * @param appId
     * @return Returns a [Response] object wrapped to [Observable]
     */
    @GET("data/2.5/weather")
    fun getWeatherInfoByCoords(@Query("lat") latitude: Double?, @Query("lon") longitude: Double?, @Query("APPID") appId: String): Observable<Response>

    /**
     * Network call with city name as paramter
     * @param city
     * @param appId
     * @return Returns a [Response] object wrapped to [Observable]
     */
    @GET("data/2.5/weather")
    fun getWeatherInfoByCity(@Query("q") city: String, @Query("APPID") appId: String): Observable<Response>
}
