package com.adamfejes.myweather.network

import android.content.res.Resources

import com.adamfejes.myweather.R
import com.facebook.stetho.okhttp3.StethoInterceptor

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * ApiManager [ApiInterface]
 * Created by Adam Fejes on 2018. 03. 26..
 */

class ApiManager
/**
 * Builds a Retrofit instance
 * @param resources
 */
(resources: Resources) {
    private val APP_ID: String = resources.getString(R.string.app_id)
    private val BASE_URL: String = resources.getString(R.string.base_url)
    private val apiInterface: ApiInterface

    init {

        apiInterface = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClient.Builder().addNetworkInterceptor(StethoInterceptor()).build())
                .build()
                .create(ApiInterface::class.java)
    }

    /**
     * Call network with coordinates
     * @param latitude
     * @param longitude
     * @return
     */
    fun getWeatherByCoords(latitude: Double?, longitude: Double?): Observable<Response> {
        return apiInterface.getWeatherInfoByCoords(latitude, longitude, APP_ID)
                .subscribeOn(Schedulers.io())
    }

    /**
     * Call network with city name
     * @param city
     * @return
     */
    fun getWeatherByCity(city: String): Observable<Response> {
        return apiInterface.getWeatherInfoByCity(city, APP_ID)
                .subscribeOn(Schedulers.io())
    }
}
