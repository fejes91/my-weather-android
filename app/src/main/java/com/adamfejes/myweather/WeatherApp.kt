package com.adamfejes.myweather

import com.adamfejes.myweather.di.AppComponent
import com.adamfejes.myweather.di.DaggerAppComponent
import com.facebook.stetho.Stetho

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * [android.app.Application] implementation with [DaggerAppComponent] and [Stetho] initialization
 * Created by Adam_Fejes on 2018-03-26.
 */

class WeatherApp : DaggerApplication() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}
