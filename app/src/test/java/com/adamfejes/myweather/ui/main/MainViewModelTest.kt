package com.adamfejes.myweather.ui.main

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.*
import android.content.res.Resources
import android.graphics.Color
import android.location.Location
import android.view.View

import com.adamfejes.Utils
import com.adamfejes.myweather.R
import com.adamfejes.myweather.db.WeatherEntity
import com.adamfejes.myweather.location.LocationModel

import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

import java.text.DateFormat
import java.util.Date

/**
 * Created by Adam_Fejes on 2018-03-27.
 */
class MainViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val LONGITUDE = 19.0
    private val LATITUDE = 47.0

    private val DOUBLE_FORMAT = "%.1f"
    private val NORTH = "N"
    private val ICON_ID = "abcd123"
    private val WIND_SPEED = 10.0
    private val WIND_SPEED_FORMATED = String.format(DOUBLE_FORMAT, WIND_SPEED)
    private val TEMPERATURE = -2.0
    private val TEMPERATURE_FORMATED = String.format(DOUBLE_FORMAT, TEMPERATURE)
    private val CONDITION = "test description"

    private val NOW = Date()
    private val NOW_FORMATED = DateFormat.getDateTimeInstance().format(NOW)
    private val ERROR_MESSAGE = "ERROR MESSAGE"
    private val IS_LOADING = true
    private val LOCATION_REQUEST_INPROGRESS = true
    private val LOCATION_IS_AVAILABLE = true
    private val VISIBLE = View.VISIBLE
    private val GONE = View.GONE
    private val ERROR_COLOR = Color.RED
    private val LOCATION_IN_PROGRESS_COLOR = R.color.yellow


    private val WEATHER_ENTITY = WeatherEntity()

    @Mock
    internal lateinit var resources: Resources
    @Mock
    internal lateinit var location: Location
    @Mock
    private lateinit var lifecycleOwner: LifecycleOwner
    @Mock
    private lateinit var mainModel: MainModel
    @Mock
    private lateinit var locationModel: LocationModel
    @Mock
    private lateinit var utils: Utils
    @Mock
    private lateinit var view: View

    private val locationLiveData: MutableLiveData<Location> = MutableLiveData()
    private val locationRequestStatusLiveData: MutableLiveData<Boolean> = MutableLiveData()
    private val hasErrorLiveData: MutableLiveData<String> = MutableLiveData()
    private val isLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    private val weatherLiveData: MutableLiveData<WeatherEntity> = MutableLiveData()
    private lateinit var underTest: MainViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        setupLifecycleMocking()
        setupLocationMocking()
        setupWeatherMocking()

        underTest = MainViewModelImpl(lifecycleOwner, utils, mainModel, locationModel)
    }

    private fun <T> anyObject(): T {
        return Mockito.anyObject<T>()
    }

    private fun setupLifecycleMocking() {
        val lifecycle = LifecycleRegistry(lifecycleOwner)
        lifecycle.markState(Lifecycle.State.RESUMED)
        `when`(lifecycleOwner.lifecycle).thenReturn(lifecycle)
    }

    private fun setupLocationMocking() {
        `when`(utils.isOnline(anyObject())).thenReturn(true)
        `when`(resources.getColor(R.color.yellow)).thenReturn(LOCATION_IN_PROGRESS_COLOR)
        `when`(location.latitude).thenReturn(LATITUDE)
        `when`(location.longitude).thenReturn(LONGITUDE)
        locationLiveData.value = location
        locationRequestStatusLiveData.setValue(LOCATION_REQUEST_INPROGRESS)
        `when`<LiveData<Location>>(locationModel.locationLiveData).thenReturn(locationLiveData)
        `when`(locationModel.isLocationAvailable).thenReturn(LOCATION_IS_AVAILABLE)
        `when`<LiveData<Boolean>>(locationModel.inProgressLiveData).thenReturn(locationRequestStatusLiveData)
    }

    private fun setupWeatherMocking() {
        `when`(resources.getString(R.string.icon_url_pattern, ICON_ID)).thenReturn(ICON_URL)
        WEATHER_ENTITY.windDirection = NORTH
        WEATHER_ENTITY.weatherIconUrl = ICON_URL
        WEATHER_ENTITY.windSpeed = WIND_SPEED
        WEATHER_ENTITY.updatedAt = NOW
        WEATHER_ENTITY.temperature = TEMPERATURE
        WEATHER_ENTITY.condition = CONDITION

        weatherLiveData.setValue(WEATHER_ENTITY)
        isLoadingLiveData.setValue(IS_LOADING)
        hasErrorLiveData.setValue(ERROR_MESSAGE)
        `when`<LiveData<WeatherEntity>>(mainModel.weather).thenReturn(weatherLiveData)
        `when`<LiveData<Boolean>>(mainModel.isLoading).thenReturn(isLoadingLiveData)
        `when`<LiveData<String>>(mainModel.errorMessage()).thenReturn(hasErrorLiveData)
    }

    @Test
    @Throws(Exception::class)
    fun getCondition() {
        Assert.assertEquals(underTest.condition, CONDITION)
    }

    @Test
    @Throws(Exception::class)
    fun getTemperature() {
        Assert.assertEquals(underTest.temperature, TEMPERATURE_FORMATED)
    }

    @Test
    @Throws(Exception::class)
    fun getWindSpeed() {
        Assert.assertEquals(underTest.windSpeed, WIND_SPEED_FORMATED)
    }

    @Test
    @Throws(Exception::class)
    fun getWindDirection() {
        Assert.assertEquals(underTest.windDirection, NORTH)
    }

    @Test
    @Throws(Exception::class)
    fun getUpdated() {
        Assert.assertEquals(underTest.updated, NOW_FORMATED)
    }

    @Test
    @Throws(Exception::class)
    fun getProgressBarVisibility() {
        Assert.assertEquals(underTest.progressBarVisibility.toLong(), VISIBLE.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun getDateVisibilty() {
        Assert.assertEquals(underTest.dateVisibilty.toLong(), GONE.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun getDateColor() {
        Assert.assertEquals(underTest.dateColor.toLong(), ERROR_COLOR.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun getIconUrl() {
        Assert.assertEquals(underTest.iconUrl, ICON_URL)
    }

    @Test
    @Throws(Exception::class)
    fun getSatIconColor() {
        Assert.assertEquals(underTest.satIconColor.toLong(), LOCATION_IN_PROGRESS_COLOR.toLong())
    }

    @Test
    @Throws(Exception::class)
    fun fabClicked() {
        underTest.fabClicked(view)
        verify<MainModel>(mainModel).refreshDataBeforeLocation()
    }

    companion object {
        private val ICON_URL = "http://openweathermap.org/img/w/abcd123.png"
    }

}