package com.adamfejes.myweather.ui.main

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.location.Location
import com.adamfejes.myweather.db.WeatherDatabase
import com.adamfejes.myweather.db.WeatherEntity
import com.adamfejes.myweather.logic.WeatherResponseMapper
import com.adamfejes.myweather.network.ApiManager
import com.adamfejes.myweather.network.Response
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import java.util.*

/**
 * Created by Adam_Fejes on 2018-03-27.
 */
@RunWith(RobolectricTestRunner::class)
class MainModelTest {
    private val DUMMY_RESPONSE = Response()
    private val LOCATION = Location("test")
    private val LOCAL_WEATHER_ENTITY = WeatherEntity()
    private val NETWORK_WEATHER_ENTITY = WeatherEntity()
    private val NORTH = "N"
    private val SOUTH = "S"
    private val WIND_SPEED1 = 10.0
    private val WIND_SPEED2 = 23.0
    private val TEMPERATURE1 = -2.0
    private val TEMPERATURE2 = 32.0
    private val CONDITION1 = "condition1"
    private val CONDITION2 = "condition2"

    private val NOW = Date()
    private val PAST = Date(NOW.time - 10000)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var apiManager: ApiManager
    @Mock
    private lateinit var responseMapper: WeatherResponseMapper

    private lateinit var db: WeatherDatabase
    private lateinit var underTest: MainModel
    private var result: WeatherEntity? = null

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        db = Room.inMemoryDatabaseBuilder(RuntimeEnvironment.systemContext, WeatherDatabase::class.java).allowMainThreadQueries().build()

        LOCAL_WEATHER_ENTITY.windDirection = NORTH
        LOCAL_WEATHER_ENTITY.weatherIconUrl = ICON_URL1
        LOCAL_WEATHER_ENTITY.windSpeed = WIND_SPEED1
        LOCAL_WEATHER_ENTITY.updatedAt = NOW
        LOCAL_WEATHER_ENTITY.temperature = TEMPERATURE1
        LOCAL_WEATHER_ENTITY.condition = CONDITION1

        NETWORK_WEATHER_ENTITY.windDirection = SOUTH
        NETWORK_WEATHER_ENTITY.weatherIconUrl = ICON_URL2
        NETWORK_WEATHER_ENTITY.windSpeed = WIND_SPEED2
        NETWORK_WEATHER_ENTITY.updatedAt = PAST
        NETWORK_WEATHER_ENTITY.temperature = TEMPERATURE2
        NETWORK_WEATHER_ENTITY.condition = CONDITION2

        `when`(apiManager.getWeatherByCity(ArgumentMatchers.anyString())).thenReturn(Observable.just(DUMMY_RESPONSE))

        underTest = MainModelImpl(db.weatherDao(), apiManager, responseMapper)
        underTest.weather.observeForever { weatherEntity -> result = weatherEntity }
    }

    @After
    fun close() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun getWeatherFromCache() {
        db.weatherDao().storeWeather(LOCAL_WEATHER_ENTITY)
        Assert.assertEquals(result, LOCAL_WEATHER_ENTITY)
    }

    @Test
    @Throws(Exception::class)
    fun getWeatherFromNetwork() {
        db.weatherDao().storeWeather(LOCAL_WEATHER_ENTITY)
        `when`(apiManager.getWeatherByCoords(any<Double>(), any<Double>())).thenReturn(Observable.just(DUMMY_RESPONSE))
        `when`(responseMapper.mapWeatherEntity(com.nhaarman.mockitokotlin2.any())).thenReturn(NETWORK_WEATHER_ENTITY)
        underTest.refreshData(LOCATION)

        Assert.assertNotEquals(result, LOCAL_WEATHER_ENTITY)
        Assert.assertEquals(result, NETWORK_WEATHER_ENTITY)
    }

    companion object {
        private val ICON_URL1 = "http://openweathermap.org/img/w/abcd123.png"
        private val ICON_URL2 = "http://openweathermap.org/img/w/efgh456.png"

        @BeforeClass
        fun setupSuite() {
            RxJavaPlugins.setIoSchedulerHandler { schedulerCallable -> io.reactivex.schedulers.Schedulers.trampoline() }
            RxJavaPlugins.setNewThreadSchedulerHandler { schedulerCallable -> io.reactivex.schedulers.Schedulers.trampoline() }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { schedulers -> io.reactivex.schedulers.Schedulers.trampoline() }
        }
    }
}