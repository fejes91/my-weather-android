package com.adamfejes.myweather.logic

import android.content.res.Resources

import com.adamfejes.myweather.R
import com.adamfejes.myweather.db.WeatherEntity
import com.adamfejes.myweather.network.Response

import junit.framework.Assert

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.`when`

import java.util.ArrayList

/**
 * Created by Adam_Fejes on 2018-03-27.
 */
@RunWith(MockitoJUnitRunner::class)
class WeatherResponseMapperTest {

    @Mock
    internal lateinit var resources: Resources

    private lateinit var underTest: WeatherResponseMapper

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        `when`(resources.getString(R.string.icon_url_pattern, ICON)).thenReturn(ICON_URL)
        underTest = WeatherResponseMapper(resources)

        MOCK_RESPONSE.weather = ArrayList<Response.Weather>()
        val weather = Response.Weather()
        weather.description = DESCRIPTION
        weather.icon = ICON
        MOCK_RESPONSE.weather!!.add(weather)

        MOCK_RESPONSE.main = Response.Main()
        MOCK_RESPONSE.main!!.temp = ABSOLUTE_ZERO

        MOCK_RESPONSE.wind = Response.Wind()
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_NORTH
        MOCK_RESPONSE.wind!!.speed = WIND_SPEED
    }

    @Test
    @Throws(Exception::class)
    fun weatherEntityAllMapping() {
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.id, ID)
        Assert.assertEquals(entity.windSpeed, WIND_SPEED)
        Assert.assertEquals(entity.temperature, ABSOLUTE_ZERO_IN_CELSIUS)
        Assert.assertEquals(entity.condition, DESCRIPTION)
        Assert.assertEquals(entity.weatherIconUrl, ICON_URL)
    }

    @Test
    @Throws(Exception::class)
    fun windDirectionMappingNorth() {
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_NORTH
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.windDirection, NORTH)
    }

    @Test
    @Throws(Exception::class)
    fun windDirectionMappingNorthEast() {
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_NORTH_EAST
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.windDirection, NORTH_EAST)
    }

    @Test
    @Throws(Exception::class)
    fun windDirectionMappingNorthWest() {
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_NORTH_WEST
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.windDirection, NORTH_WEST)
    }

    @Test
    @Throws(Exception::class)
    fun windDirectionMappingEast() {
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_EAST
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.windDirection, EAST)
    }

    @Test
    @Throws(Exception::class)
    fun windDirectionMappingSouthEast() {
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_SOUTH_EAST
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.windDirection, SOUTH_EAST)
    }

    @Test
    @Throws(Exception::class)
    fun windDirectionMappingSouthWest() {
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_SOUTH_WEST
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.windDirection, SOUTH_WEST)
    }

    @Test
    @Throws(Exception::class)
    fun windDirectionMappingSouth() {
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_SOUTH
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.windDirection, SOUTH)
    }

    @Test
    @Throws(Exception::class)
    fun windDirectionMappingWest() {
        MOCK_RESPONSE.wind!!.deg = WIND_DEG_WEST
        val entity = underTest.mapWeatherEntity(MOCK_RESPONSE)
        Assert.assertEquals(entity.windDirection, WEST)
    }

    companion object {
        private val MOCK_RESPONSE = Response()
        private val DESCRIPTION = "test description"
        private val ICON = "abcd123"
        private val ICON_URL = "http://openweathermap.org/img/w/abcd123.png"
        private val ABSOLUTE_ZERO = 0.0
        private val WIND_SPEED = 10.0
        private val ABSOLUTE_ZERO_IN_CELSIUS = -273.15
        private val ID = 0L

        private val WIND_DEG_NORTH = 0.0
        private val WIND_DEG_NORTH_EAST = 45.0
        private val WIND_DEG_EAST = 90.0
        private val WIND_DEG_SOUTH_EAST = 135.0
        private val WIND_DEG_SOUTH = 180.0
        private val WIND_DEG_SOUTH_WEST = 225.0
        private val WIND_DEG_WEST = 270.0
        private val WIND_DEG_NORTH_WEST = 315.0
        private val NORTH = "N"
        private val NORTH_EAST = "NE"
        private val NORTH_WEST = "NW"
        private val EAST = "E"
        private val SOUTH = "S"
        private val SOUTH_EAST = "SE"
        private val SOUTH_WEST = "SW"
        private val WEST = "W"
    }

}