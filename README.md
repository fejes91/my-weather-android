# Android demo weather app #

Tech stack:

* Retrofit
* RxJava2
* Databinding
* LiveData
* Room
* MVVM

App downloads weather info based on the current location from openweathermap.org API. Stores it to local Room DB for offline usage.